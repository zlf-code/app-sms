<?php
declare(strict_types=1);

namespace Zlf\Sms;

interface SmsInterface
{
    /**
     * 获取短信余额
     * @return int
     */
    public function getBalance(): int;


    /**
     * 发送短信
     * @param string $phone 手机号
     * @param string $template_key 短信模板调用名称
     * @param array $data 传入参数
     * @return SmsResult 发送结果
     */
    public function send(string $phone, string $template_key, array $data = []): SmsResult;

}