<?php
declare(strict_types=1);

namespace Zlf\Sms\Drive\Config;
/**
 * 阿里云短信配置
 * @link  http://cf.82009668.com
 */
class Alconfig extends ConfigBasics
{
    /**
     * 请求域名
     * @var string
     */
    public string $host = 'https://dysmsapi.aliyuncs.com';

    public string $AccessKeyId;

    public string $AccessKeySecret;


    /**
     * @param string $AccessKeyId 阿里云授权ID
     * @param string $AccessKeySecret 阿里云授权密钥
     * @param string $signature 短信签名
     * @param array $template 短信模板
     */
    public function __construct(string $AccessKeyId, string $AccessKeySecret, string $signature, array $template)
    {
        $this->AccessKeyId = $AccessKeyId;
        $this->AccessKeySecret = $AccessKeySecret;
        $this->signature = $signature;
        $this->template = $template;
    }
}