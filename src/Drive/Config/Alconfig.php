<?php
declare(strict_types=1);

namespace Zlf\Sms\Drive\Config;

/**
 * 阿里云短信配置类
 * 提供了阿里云短信服务所需的配置信息。
 */
class Alconfig extends ConfigBasics
{
    /**
     * 请求域名
     * 用于发送短信请求的API域名。
     *
     * @var string 默认为阿里云短信服务的API地址
     */
    public $host = 'https://dysmsapi.aliyuncs.com';

    /**
     * 阿里云授权ID
     * 用于身份验证的AccessKeyId。
     *
     * @var string
     */
    public $AccessKeyId;

    /**
     * 阿里云授权密钥
     * 与AccessKeyId配合使用的密钥，用于签名请求。
     *
     * @var string
     */
    public $AccessKeySecret;

    /**
     * 短信签名
     * 发送短信时附带的签名信息。
     *
     * @var string
     */
    public $signature;

    /**
     * 短信模板
     * 包含短信模板的相关信息，如模板代码、变量等。
     *
     * @var array
     */
    public $template;

    /**
     * 构造函数
     * 初始化阿里云短信配置实例。
     *
     * @param string $AccessKeyId 阿里云授权ID
     * @param string $AccessKeySecret 阿里云授权密钥
     * @param string $signature 短信签名
     * @param array $template 短信模板
     */
    public function __construct(string $AccessKeyId, string $AccessKeySecret, string $signature, array $template)
    {
        $this->AccessKeyId = $AccessKeyId;
        $this->AccessKeySecret = $AccessKeySecret;
        $this->signature = $signature;
        $this->template = $template;
    }
}