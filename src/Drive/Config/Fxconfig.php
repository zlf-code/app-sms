<?php
declare(strict_types=1);

namespace Zlf\Sms\Drive\Config;
/**
 * 成都德昭分享科技有限公司
 * @link  http://cf.82009668.com
 */
class Fxconfig extends ConfigBasics
{
    /**
     * 请求域名
     * @var string
     */
    public string $host = 'http://cf.82009668.com:888';

    public string $username;

    public string $password;


    /**
     * @param string $username 短信账号
     * @param string $password 短信密码
     * @param string $signature 短信签名
     * @param array $template 短信模板
     */
    public function __construct(string $username, string $password, string $signature, array $template)
    {
        $this->username = $username;
        $this->password = $password;
        $this->signature = $signature;
        $this->template = $template;
    }

}