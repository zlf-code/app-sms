<?php
declare(strict_types=1);

namespace Zlf\Sms\Drive\Config;
/**
 * 助通短信配置类
 * 提供了使用助通科技短信服务所需的配置信息。
 * @link http://cf.82009668.com 参考链接
 */
class Ztconfig extends ConfigBasics
{
    /**
     * 请求域名
     * 用于发送短信请求的API地址。
     *
     * @var string 默认为助通科技提供的API地址
     */
    public $host = 'https://api.mix2.zthysms.com';

    /**
     * 短信账号
     * 在助通科技注册的短信服务账号。
     *
     * @var string
     */
    public $username;

    /**
     * 短信密码
     * 对应短信账号的密码，用于身份验证。
     *
     * @var string
     */
    public $password;

    /**
     * 构造函数
     * 初始化Ztconfig实例，并设置必要的配置参数。
     *
     * @param string $username 短信账号
     * @param string $password 短信密码
     * @param string $signature 短信签名
     * @param array $template 短信模板数组，包含一个或多个短信模板的信息
     */
    public function __construct(string $username, string $password, string $signature, array $template)
    {
        $this->username = $username;
        $this->password = $password;
        $this->signature = $signature;
        $this->template = $template;
    }
}