<?php
declare(strict_types=1);

namespace Zlf\Sms\Drive\Config;
/**
 * 助通短信配置
 * @link  http://cf.82009668.com
 */
class Ztconfig extends ConfigBasics
{
    /**
     * 请求域名
     * @var string
     */
    public string $host = 'https://api.mix2.zthysms.com';

    public string $username;

    public string $password;


    /**
     * @param string $username 短信账号
     * @param string $password 短信密码
     * @param string $signature 短信签名
     * @param array $template 短信模板
     */
    public function __construct(string $username, string $password, string $signature, array $template)
    {
        $this->username = $username;
        $this->password = $password;
        $this->signature = $signature;
        $this->template = $template;
    }

}