<?php
declare(strict_types=1);

namespace Zlf\Sms\Drive\Config;

/**
 * 短信模板基础配置类
 * 提供了短信服务所需的基础配置信息，包括短信签名和短信模板。
 */
class ConfigBasics
{
    /**
     * 短信签名
     * 用于发送短信时附带的签名信息，通常为公司或品牌名称。
     *
     * @var string 签名字符串
     */
    public $signature;

    /**
     * 短信模板
     * 包含一个或多个短信模板的信息。每个模板包含模板调用名称、服务商对应的模板ID、模板描述以及模板内容。
     *
     * 示例：
     * [
     *   [
     *     'KEY' => 'SMST-DEFAULT',
     *     'TEMPLATE_ID' => 'SMS_548784587',
     *     'DESCRIPTION' => '短信验证码',
     *     'CONTENT' => '【{signature}】您的验证码为：{code}，请勿泄露于他人！'
     *   ]
     * ]
     * 其中，
     * - KEY: 短信模板调用名称
     * - TEMPLATE_ID: 服务商对应的模板ID
     * - DESCRIPTION: 模板描述
     * - CONTENT: 模板内容，其中 {signature} 和 {code} 是变量占位符
     *
     * @var array 短信模板数组
     */
    public $template = [];
}