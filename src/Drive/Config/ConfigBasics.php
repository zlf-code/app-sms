<?php
declare(strict_types=1);

namespace Zlf\Sms\Drive\Config;

/**
 * 短信模板
 */
class ConfigBasics
{
    /**
     * 短信签名
     * @var string
     */
    public string $signature;


    /**
     * 短信模板
     * @example [
     *              ['KEY' => 'SMST-DEFAULT', 'TEMPLATE_ID' => 'SMS_548784587','DESCRIPTION' => '短信验证码', 'CONTENT' => '【{signature}】您的验证码为：{code}，请勿泄露于他人！']
     *          ]
     * @example    KEY 短信模板调用名称  TEMPLATE_ID 服务商对应的模板ID   DESCRIPTIO模板描述  CONTENT模板内容
     * @var array
     */
    public array $template = [];

}