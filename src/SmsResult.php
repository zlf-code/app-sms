<?php
declare(strict_types=1);

namespace Zlf\Sms;

/**
 * 短信发送返回实例
 * 该类用于封装短信发送的结果信息，包括发送的手机号、内容、状态、使用的驱动程序、模板名称以及SDK返回的数据。
 */
class SmsResult
{
    /**
     * 发送的手机号码
     *
     * @var string 手机号码字符串
     */
    public $phone;

    /**
     * 发送的短信内容
     *
     * @var string 短信内容字符串
     */
    public $content;

    /**
     * 发送状态
     * 表示短信是否成功发送。
     *
     * @var bool 如果发送成功则为true，否则为false
     */
    public $status;

    /**
     * 使用的短信驱动名称
     *
     * @var string 驱动程序的类名或标识符
     */
    public $driver;

    /**
     * 模板名称
     *
     * @var string 短信模板的唯一标识符或名称
     */
    public $template_key;

    /**
     * SDK返回的数据
     * 包含从短信服务提供商的SDK接收到的所有响应数据。
     *
     * @var array|null 可能包含响应状态码、消息ID等信息，如果无额外数据则为null
     */
    public $request = null;

    /**
     * 构造函数
     * 根据提供的结果数组初始化SmsResult对象的属性。
     *
     * @param array $result 结果数组，键值对形式，键对应属性名，值对应属性值
     */
    public function __construct(array $result)
    {
        foreach ($result as $attr => $value) {
            // 设置当前对象的属性值
            $this->$attr = $value;
        }
    }
}