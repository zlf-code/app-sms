<?php
declare(strict_types=1);

namespace Zlf\Sms;

/**
 * 短信发送返回实例
 */
class SmsResult
{
    /**
     * @var string 发送的手机号
     */
    public string $phone;
    /**
     * 发送的短信内容
     * @var string
     */
    public string $content;
    /**
     * 发送状态
     * @var bool
     */
    public bool $status;
    /**
     * 短信驱动
     * @var string
     */
    public string $driver;
    /**
     * 模板名称
     * @var string
     */
    public string $template_key;

    /**
     * SDK返回的数据
     * @var array|null
     */
    public ?array $request = null;

    public function __construct(array $result)
    {
        foreach ($result as $attr => $value) {
            $this->$attr = $value;
        }
    }
}