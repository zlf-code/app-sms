<?php
declare(strict_types=1);

namespace Zlf\Sms;

use Zlf\AppException\Exception\AppException;
use Zlf\Sms\Drive\Alsms;
use Zlf\Sms\Drive\Fxsms;
use Zlf\Sms\Drive\SmsBasics;
use Zlf\Sms\Drive\Ztsms;

class Sms
{
    private array $_instance;//短信实例

    /**
     * 获取短信实例
     * @param string $drive 短信驱动程序
     * @param callable $callable 配置获取回调
     * @return SmsBasics
     * @throws AppException
     */
    public function instance(string $drive, callable $callable): SmsBasics
    {
        $this->checkDrive($drive);
        if (!(isset($this->_instance[$drive]) && $this->_instance[$drive] instanceof SmsBasics)) {
            /**
             * @var SmsBasics $driveInstance
             */
            $driveInstance = new $drive;
            $driveInstance->setConfigCallable($callable);
            $this->_instance[$drive] = $driveInstance;
        }
        return $this->_instance[$drive];
    }


    public static function drives(): array
    {
        return [
            ['drive' => Alsms::class, 'desc' => '阿里云短信'],
            ['drive' => Ztsms::class, 'desc' => '助通科技短信'],
            ['drive' => Fxsms::class, 'desc' => '分享短信']
        ];
    }


    /**
     * 获取驱动名称
     * @param string $drive
     * @return string
     */
    public static function getDriveName(string $drive): string
    {
        foreach (self::drives() as $item) {
            if ($item['drive'] === $drive) {
                return $item['desc'];
            }
        }
        return '';
    }


    /**
     * 检查短信驱动
     * @param string $drive
     * @return void
     * @throws AppException
     */
    private function checkDrive(string $drive): void
    {
        foreach (self::drives() as $item) {
            if ($item['drive'] === $drive) {
                return;
            }
        }
        throw new AppException("短信驱动错误");
    }

}