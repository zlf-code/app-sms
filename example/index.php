<?php
declare(strict_types=1);

use Zlf\Sms\Drive\Alsms;

//使用对应的短信驱动
use Zlf\Sms\Drive\Config\Alconfig;

//载入对应的短信配置
use Zlf\Sms\Drive\Config\Fxconfig;
use Zlf\Sms\Drive\Config\Ztconfig;
use Zlf\Sms\Drive\SmsBasics;
use Zlf\Sms\Drive\Ztsms;
use Zlf\Sms\Sms;

require_once "../vendor/autoload.php";


/**
 * 获取短信实例
 */
function instance(): SmsBasics
{
    $sms = new Sms;

    $configure = [
        'open' => 1,
        'AccessKeyId' => 'xxxxxxxx',
        'AccessKeySecret' => 'xxxxxxxx',
        'sign' => '腾讯科技',
    ];

    $template = [
        ['KEY' => 'SMST-DEFAULT', 'TEMPLATE_ID' => 'SMS_472450418', 'DESCRIPTION' => '验证码', 'CONTENT' => '【{signature}】您的验证码为：{code}，请不要露给他人！']
    ];

    /**
     * 获取短信实例
     */
    return $sms->instance(Alsms::class, function (string $drive) use ($configure, $template) {
        if ($drive === Alsms::class) {
            //阿里云短信
            return new Alconfig($configure['AccessKeyId'], $configure['AccessKeySecret'], $configure['sign'], $template);
        } elseif ($drive === Ztsms::class) {
            //助通短信科技
            return new Ztconfig($configure['username'], $configure['password'], $configure['sign'], $template);
        }
        //分享短信
        return new Fxconfig($configure['username'], $configure['password'], $configure['sign'], $template);
    });
}


$instance = instance();

$result=$instance->send('188xxxxxxxx', 'SMST-DEFAULT', ['code' => 3302]);
print_r($result);