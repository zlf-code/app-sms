# 短信组件包

    需要支持php>=8，其他版本自行修改

#### 支持类型

    ['drive' => Alsms::class, 'desc' => '阿里云短信'],
    ['drive' => Ztsms::class, 'desc' => '助通科技短信'],
    ['drive' => Fxsms::class, 'desc' => '分享短信']

#### 使用示例

    <?php
    declare(strict_types=1);
    
    use Zlf\Sms\Drive\Alsms;
    
    //使用对应的短信驱动
    use Zlf\Sms\Drive\Config\Alconfig;
    
    //载入对应的短信配置
    use Zlf\Sms\Sms;
    
    require_once "../vendor/autoload.php";
    
    $sms = new Sms;
    
    /**
     * 获取短信实例
     */
    $sms = $sms->instance(Alsms::class, function (string $drive) {
        /**
         * 返回对应的短信配置实例
         */
        return new Alconfig('xxxx', 'xxxx', 'XX公司', [
            ['KEY' => 'SMST-DEFAULT', 'TEMPLATE_ID' => 'SMS_284970217', 'DESCRIPTION' => '短信验证码', 'CONTENT' => '【{signature}】您的验证码为：{code}，请勿泄露于他人！']
        ]);
    });
    
    //发送短信
    $sms->send('18777777777', 'SMST-DEFAULT', ['code' => '99878']);
    
    //查询余额
    $sms->getBalance();